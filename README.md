
Proyecto desarrollado para el Trabajo Practico Final de la materia Autómatas, Teoría de Lenguajes y Compiladores.

Integrantes del grupo:
	Oliva Juan Martin - 58664
	Vindis Davor - 58663
	Neuss Julian Jorge - 59266
	Maspero Geronimo - 58322

Coloscript es un lenguaje de programación orientado a la generación de juegos de manera simple y rápida. Este lenguaje creado se considera muy facil de aprender y usar. Facilita la creación de juegos a través de las simples líneas de código  con variables con nombres muy intuitivos para permitir a cualquier usuario hacer un juego funcionar.

Para correr un juego en Coloscript es necesario proccessing-java. Una vez instalado processing, el siguiente script compilará y ejecutará un juego de ejemplo:

	./coloscript.sh example_games/cross_the_street

En la carpeta example_games se encuentran otros códigos de ejemplo.

Para descargar processing java hay que seguir las instrucciones del siguiente link https://processing.org/download/











































