
#define GLOBAL_SCOPE 0
#define LOCAL_SCOPE 1

enum color{RED = 0, YELLOW, BLACK, WHITE, ORANGE, PURPLE, PINK, GREEN, LIGHT_BLUE, BLUE  };

struct position{
        char x[50];
        char y[50]; 
    };

enum variable_types{
    INTEGER = 0,
    BLOCK,
    POSITION
};

enum game_steps{
    SETUP = 0,
    GAME,
    USER_FUNCTIONS
};

struct block_variable {
       char identifier[100];
       char block_color[15];
       struct position pos ;
       char x_size[50];
       char y_size[50];

       struct block_variable* next;
    };

  

struct identifier_list_node {
    char identifier[100];

    struct identifier_list_node *next;
};

struct move_list_node {
    char block_id[100];
    char block_direction[15];
    char block_attributes[50];
    
    struct move_list_node *next;
};

struct role_list_node {
    char blocks_ids[500];
    char role_id[100];

    struct role_list_node *next;
};

struct addresses_list {
    char *address;

    struct addresses_list *next;
};

int variable_in_global_scope(char *identifier);

int variable_in_current_scope(char *identifier);

int add_variable_to_current_scope(char *identifier, enum variable_types type);

int update_variable_value(char *identifier, enum variable_types type, void *variable_content);

char* colors_initializer ();

int get_variable_value(char *identifier, enum variable_types type, void *variable_content);

int identifier_is_block_or_role(char *identifier);

int add_role_if_not_exists(char *identifier);

void add_function_declaration(char* fname);

void add_function_calls(char* fname);

int function_already_defined(char* fname);

int verify_functions_called();

int is_defined_int_variable(char* identifier);

void free_pendings();

void free_setup_data();

void free_function_structures();

int add_address_to_free_queue(char *address);