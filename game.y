		   %{
		    /* Este bloque aparecerá tal cual en el fichero yy.lex.c */

		    /* ----------------  C declarations used in actions -------------------------------- */

		    void yyerror (char *s);
		    int yylex();
		    #include <stdio.h>     
		    #include <stdlib.h>
		    #include <string.h>
		    #include "game_utils.h"
		    

		    struct position_variable {
			char* identifier;
			struct position value;
		    };

		    struct variables {
			char *identifier;

			enum variable_types variable_type;
			
			struct variables* next;
		    };


	    struct variables* global_scope_variables;
	    struct variables* current_scope_variables;

	    int processing_current_scope;

	    enum game_steps game_step;

	    struct identifier_list_node *setup_links;
        struct identifier_list_node *setup_links_wasd;
        struct identifier_list_node *defined_roles;
        struct move_list_node * setup_moves;
        struct role_list_node * setup_roles;
        struct block_variable * setup_blocks;

        struct identifier_list_node *functions_called;
        struct identifier_list_node *functions_defined;


        struct addresses_list *pending_frees;
	    %}
	    /* ----------------  C declarations used in actions -------------------------------- */




	    /* ---------------- Yacc definitions -------------------------------- */
	    %union {int num; char* id;}  /* Yacc Definitions*/
	    %start Programa

	    %token setup
	    %token end
	    %token Integer 
	    %token while_
	    %token Block
	    %token move
	    %token when
	    %token Position
	    %token Link
	    %token flechita
	    %token rect
	    %token game
	    %token Igualdad
	    %token Diferente
	    %token MenorIgual
	    %token MayorIgual
	    %token randomnum
	    %token role
	    %token mouse
        %token flechas_direccion
	    %token IF
	    %token TO
	    %token speed
	    %token give
	    %token lose
	    %token win
	    %token gameover
	    %token reset
	    %token bounces
	    %token right
	    %token up
	    %token left
	    %token down

	    %token hits
	    %token over
	    %token on
	    %token under
	    %token touches


	    %token yellow                                     
        %token red                                          
        %token blue                                         
        %token black                                          
        %token grey                                         
        %token white                                          
        %token orange                                         
        %token pink                                          
        %token purple                                         
        %token light_blue                                           
        %token green                                         

    
    %token <num> DIGIT
    %token <id> IDENTIFIER 
    

    //new types added:
    %type <id> NUMBER OP 
    %type <id> ATTRIBUTES COLOR OPER EV WCOND COND DIR ATR1 ID Fbody F Declaration Instruction Assignment GameInstruction 
    /////////////////

    %type <id> Se 
  


    /* ---------------- Yacc definitions --------------------------------*/
    %%
    
    
    Programa: Se Ga LF                     { //verifar que todas las funciones llamadas en Ga existen en LF
                                             if (!verify_functions_called()) { 
                                                  
                                                yyerror("A function was called that was never declared!");
                                             };
                                             free_setup_data(); 
                                             free_function_structures();
                                            
                                            ;}
	;
	Se: setup ':' SetupFbody end '.'             { // sacamos el scope global
                                                printf("void setup(){\n  \
                                                    size(640, 460); \
                                                    f = createFont(\"Arial\",16,true);  \
                                                    if (win){ \
                                                    background(15, 255, 30);  \
                                                    textFont(f,16); \
                                                    fill(255); \
                                                    textAlign(CENTER); \
                                                    text(\"You Won!\",width/2,200);\
                                                    frameRate(0);} \
                                                    if (lose){  \
                                                    background(255, 180, 1); \
                                                    textFont(f,16); fill(255);\
                                                    textAlign(CENTER); \
                                                    text(\"You Lost!\",width/2,200);  \
                                                    frameRate(0);\
                                                    }");
                                                
                                                free_pendings();


                                                struct block_variable* blocks = setup_blocks;

                                                while( blocks != NULL ) {
                                                    printf("add_block( \"%s\",  %s, %s, %s, %s, 0, 0, 0, \"%s\"); \n", blocks->identifier, blocks->pos.x, blocks->pos.y, blocks->x_size, blocks->y_size, blocks->block_color); 
                            
                                                    blocks = blocks->next;
                                                };



                                                //AGREGAR MOVE
                                                ///  move("red", "up", 5, "bounce");
                                               // 

                                               struct role_list_node* roles = setup_roles;

                                                while( roles != NULL ) {
                                                    printf("give_role(\"%s\", \"%s\"); \n", roles->blocks_ids, roles->role_id); 
                            
                                                    roles = roles->next;
                                                };
                                                



                                                struct move_list_node* moves = setup_moves;

                                                while( moves != NULL ) {
                                                    printf("move(\"%s\", \"%s\", %s); \n", moves->block_id, moves->block_direction, moves->block_attributes); 
                            
                                                    moves = moves->next;
                                                };
                                                
                                                printf("}\n");

                                                processing_current_scope = LOCAL_SCOPE;
                                                global_scope_variables = current_scope_variables;
                                                current_scope_variables = NULL;
                                                game_step = GAME;
                                               printf("void draw(){   \
                                                 if (keyPressed) { \
                                                      if (key == 'w' || key == 'a' || key == 's' ||  key == 'd' ) {\
                                                     check_key_move();}\
                                                     }\
                                                   background(0);    \n \
                                                   for (Map.Entry<String, ArrayList<Integer>> me : bloques_data.entrySet()) {\n \
                                                        check_movement(me.getValue());\n   \
							                            fill(me.getValue().get(10), me.getValue().get(11) ,me.getValue().get(12)); \  
							                            rect(me.getValue().get(0), me.getValue().get(1),me.getValue().get(4),me.getValue().get(5));\
                                                    }");
                                                    
                                                struct identifier_list_node *links = setup_links;
                                                while( links != NULL ) {
                                                       printf("link(\"%s\");\n", links->identifier); 
                                                    links = links->next;
                                                }    


                                                links = setup_links_wasd;
                                                while( links != NULL ) {
                                                       printf("link_keyboard(\"%s\");\n", links->identifier); 
                                                    links = links->next;
                                                }

                                                 
                                                     }
	;
    Ga: game ':' GameFbody end '.'          { printf("}"); current_scope_variables = NULL ; free_pendings();}                                                
    ;
	Fbody: Declaration Fbody                {   char *body = malloc(4096);
                                                add_address_to_free_queue(body);
                                                strncpy(body,$1,4096);
                                                strncat(body, $2,4096);
                                                $$ = body;
        
                                                /* char monster[4096];
                                                strncpy(monster,$1,strlen($1));
                                                strncat(monster, $2,strlen($2));
                                                $$ = monster; */}
	 	 | Instruction Fbody	            {char *body = malloc(4096);
                                                add_address_to_free_queue(body);
                                                strncpy(body,$1,4096);
                                                strncat(body, $2,4096);
                                                $$ = body;}
          | Assignment  Fbody                {char *body = malloc(4096);
                                                add_address_to_free_queue(body);
                                                strncpy(body,$1,4096);
                                                strncat(body, $2,4096);
                                                $$ = body;}
         |IDENTIFIER '.' Fbody                 {add_function_calls($1);
                                                char *body = malloc(4096);
                                                add_address_to_free_queue(body);
                                                strncpy(body,$1,4096);
                                                strncat(body, $3,4096);
                                                $$ = body;}
          |                           { $$ = ";";}
	 ;
    GameFbody: Declaration GameFbody        { printf("%s", $1); }
		 | GameInstruction GameFbody	    {;}
         | Assignment GameFbody             {printf("%s", $1);}
         //| FunctionId GameFbody             {;}
         |                            {;}
	;
    SetupFbody: Declaration SetupFbody           { printf("%s", $1);
                                                    }
		 | SetupInstruction SetupFbody           {;}
         |                            {;}
	;
    GameInstruction: when WCOND flechita win '.'                 {  printf("if ( %s ){ win();} ", $2);}
                    | when WCOND flechita lose '.'               {printf("if ( %s ){ lose();} ", $2);}
                    | Link IDENTIFIER TO mouse '.'                { printf("link(\"%s\");\n", $2);}
                    | when WCOND flechita IDENTIFIER '.'          { add_function_calls($4);
                                                                    printf("if ( %s ){ %s();} ", $2,$4);}
                    |  IF '(' COND ')' '{'win'.''}'                  {  printf("if ( %s ){ win();} ", $3);}
                    |  IF '(' COND ')' '{'lose'.''}'                  {printf("if ( %s ){ lose();} ", $3);}
                    |  IF '(' COND ')' '{'IDENTIFIER'.''}'                  {char *aux = malloc(500);
                                                                    add_address_to_free_queue(aux);
                                                                    add_function_calls($6);
                                                                    snprintf(aux, 500 , "if(%s){ %s(); };" ,$3,$6);
                                                                $$ = aux;}
                    
    ; 
    SetupInstruction: give ID role IDENTIFIER '.'                 { //add Role array.
                                                                    // printf("ArrayList<String> obstacle = new ArrayList();", %4);
                                                                    add_role_if_not_exists($4);
                                                                    
                                                                    if(setup_roles == NULL ){
                                                                                //tengo que crear setup_roles.
                                                                                setup_roles = malloc(sizeof(struct role_list_node));
                                                                                if( setup_roles == NULL ){
                                                                                    yyerror("malloc error.");
                                                                                    return -1;
                                                                                }
                                                                                memset(setup_roles, 0, sizeof(struct role_list_node));

                                                                                strcpy(setup_roles->blocks_ids, $2);
                                                                                strcpy(setup_roles->role_id, $4);
                                                                             } else {
                                                                                //ya habia un setup_roles existente.
                                                                                struct role_list_node * role_var = setup_roles;
                                                                                while(role_var->next != NULL ) {
                                                                                    role_var = role_var->next;
                                                                                }
                                                                                role_var->next = malloc(sizeof(struct role_list_node));
                                                                                if( role_var->next == NULL ){
                                                                                    yyerror("malloc error.");
                                                                                    return -1;
                                                                                }

                                                                                memset(role_var->next, 0, sizeof(struct role_list_node));

                                                                                strcpy(role_var->next->blocks_ids, $2);
                                                                                strcpy(role_var->next->role_id, $4);
                                                                             }
                                                                        }
                                                           
                    | move IDENTIFIER DIR ',' ATTRIBUTES '.'          { if(!identifier_is_block_or_role($2)){  
                                                                            yyerror("ERROR: move must be applied to a block variable");
                                                                        } 

                                                                        else{ 
                                                                        // move should be set on setup.
                                                                            if(setup_moves == NULL ){
                                                                                //tengo que crear setup_moves.
                                                                                setup_moves = malloc(sizeof(struct move_list_node));
                                                                                if( setup_moves == NULL ){
                                                                                    yyerror("malloc error.");
                                                                                    return -1;
                                                                                }
                                                                                memset(setup_moves, 0, sizeof(struct move_list_node));

                                                                                strcpy(setup_moves->block_id, $2);
                                                                                strcpy(setup_moves->block_direction, $3);
                                                                                strcpy(setup_moves->block_attributes, $5);
                                                                             } else {
                                                                                //ya habia un setup_moves existente.
                                                                                struct move_list_node * mov = setup_moves;
                                                                                while(mov->next != NULL ) {
                                                                                    mov = mov->next;
                                                                                }
                                                                                mov->next = malloc(sizeof(struct move_list_node));
                                                                                if( mov->next == NULL ){
                                                                                    yyerror("malloc error.");
                                                                                    return -1;
                                                                                }

                                                                                memset(mov->next, 0, sizeof(struct move_list_node));

                                                                                strcpy(mov->next->block_id, $2);
                                                                                strcpy(mov->next->block_direction, $3);
                                                                                strcpy(mov->next->block_attributes, $5);
                                                                                }
                                                                      }    
                                                            }
                                                                        
                    | Link IDENTIFIER TO mouse '.'                { if(!identifier_is_block_or_role($2)) {
                                                                        yyerror("ERROR: link must be applied to a block variable");
                                                                     }
                                                                    else {
                                                                        // link should be set on setup.
                                                                        if(setup_links == NULL ){
                                                                            setup_links = malloc(sizeof(struct identifier_list_node));
                                                                            memset(setup_links, 0, sizeof(struct identifier_list_node));
                                                                            strncat(setup_links->identifier, $2, strlen($2));
                                                                        } else {
                                                                            struct identifier_list_node *link = setup_links;
                                                                            while(link->next != NULL ) {
                                                                                link = link->next;
                                                                            }
                                                                            link->next = malloc(sizeof(struct identifier_list_node));
                                                                            memset(link->next, 0, sizeof(struct identifier_list_node));
                                                                            strncat(link->next->identifier, $2, strlen($2));
                                                                        }

                                                                    }
                                                                        ;}
                    | Link IDENTIFIER TO flechas_direccion '.'               { if(!identifier_is_block_or_role($2)) {
                                                                        yyerror("ERROR: link must be applied to a block variable");
                                                                     }
                                                                    else {
                                                                        // link should be set on setup.
                                                                        if(setup_links_wasd == NULL ){
                                                                            setup_links_wasd = malloc(sizeof(struct identifier_list_node));
                                                                            memset(setup_links_wasd, 0, sizeof(struct identifier_list_node));
                                                                            strncat(setup_links_wasd->identifier, $2, strlen($2));
                                                                        } else {
                                                                            struct identifier_list_node *link = setup_links_wasd;
                                                                            while(link->next != NULL ) {
                                                                                link = link->next;
                                                                            }
                                                                            link->next = malloc(sizeof(struct identifier_list_node));
                                                                            memset(link->next, 0, sizeof(struct identifier_list_node));
                                                                            strncat(link->next->identifier, $2, strlen($2));
                                                                        }

                                                                    }
                                                                        }
    ;
   LF: F LF                               { current_scope_variables = NULL ; free_pendings();}
        |                            {;}
    ;
    F: IDENTIFIER ':' Fbody end '.'        {if( function_already_defined($1) ){
                                                                    char state[50];
                                                                 sprintf(state, "ERROR: function %s is already defined.", $1);
                                                                 yyerror(state); }
                                            else{
                                                add_function_declaration($1);
                                                printf("void %s(){%s};",$1, $3);
                                            }
                                                       ;}
    ;
	Declaration:	 Block IDENTIFIER '=' rect'('COLOR','NUMBER','NUMBER','NUMBER','NUMBER')' '.' {
                                                            if( variable_in_current_scope($2) ) {
                                                                // ERROR VARIABLE YA DEFINIDA.
                                                                 char state[50];
                                                                 sprintf(state, "ERROR: block variable %s is already defined.", $2);
                                                                 yyerror(state);
                                                            }
                                                            else {                                                  
                                                                
                                                                // agregar variable en struct de scope actual.
                                                               add_variable_to_current_scope($2, BLOCK); 
                                                               
                                                               if (game_step == SETUP ) {
                                                                   // block will be declared later.
                                                                    if(setup_blocks == NULL ){
                                                                            setup_blocks = malloc(sizeof(struct block_variable));
                                                                            memset(setup_blocks, 0 , sizeof(struct block_variable));
                                                                            strncpy(setup_blocks->identifier, $2, strlen($2));
                                                                            strncpy(setup_blocks->block_color, $6, strlen($6));
                                                                            strncpy(setup_blocks->pos.x, $8, strlen($8));
                                                                            strncpy(setup_blocks->pos.y , $10, strlen($10));
                                                                            strncpy(setup_blocks->x_size , $12, strlen($12));
                                                                            strncpy(setup_blocks->y_size , $14, strlen($14));  
                                                                        } else {
                                                                            struct block_variable *blocks = setup_blocks;
                                                                            while(blocks->next != NULL ) {
                                                                                blocks = blocks->next;
                                                                            }
                                                                            blocks->next = malloc(sizeof(struct block_variable));
                                                                            memset(blocks->next, 0, sizeof(struct block_variable));
                                                                            strncpy(blocks->next->identifier, $2, strlen($2));
                                                                            strncpy(blocks->next->block_color, $6, strlen($6));
                                                                            strncpy(blocks->next->pos.x, $8, strlen($8));
                                                                            strncpy(blocks->next->pos.y , $10, strlen($10));
                                                                            strncpy(blocks->next->x_size , $12, strlen($12));
                                                                            strncpy(blocks->next->y_size , $14, strlen($14));  
                                                                        }
                                                                    $$ = "";
                                                               } else {
                                                                   // codigo que agrega bloque a HASHMAP.
                                                                   char *wcond = malloc(500);
                                                                    add_address_to_free_queue(wcond);
                                                                    snprintf(wcond, 500 , "add_block( \"%s\",  %s, %s, %s, %s, 0, 0, 0, \"%s\");" , $2, $8 ,$10 ,$12 ,$14 ,$6);
                                                                $$ = wcond;
                                                                
                                                               }
                                                            }
                                                                  }
                                                                 
				|    Position IDENTIFIER '=' '('OP',' OP')''.'   { 
                                                            if( variable_in_current_scope($2) ) {
                                                                // ERROR VARIABLE YA DEFINIDA.
                                                                char state[50];
                                                                 sprintf(state,"ERROR: position variable %s is already defined.", $2);
                                                                 yyerror(state);
                                                            }
                                                

                                                            // agregar variable en struct de scope actual.
                                                            add_variable_to_current_scope($2, POSITION);


                                                            // codigo que se imprime?.
                                                             printf("int %s_position_x = %s;", $2,$5);
                                                             printf("int %s_position_y = %s;", $2,$7);
                                                            char wcond[100];
                                                            snprintf(wcond, 100,"int %s_position_x = %s ;int %s_position_y = %s; ", $2, $5 ,$2 ,$7); $$ = wcond;
                                                            ;}
                |   Integer IDENTIFIER '=' NUMBER '.' {  if( variable_in_current_scope($2) ){
                                                                // ERROR VARIABLE YA DEFINIDA.
                                                               yyerror("ERROR: This variable is already defined.");
                                                            }
                                                            else {
                                                            
                                                                
                                                                add_variable_to_current_scope($2, INTEGER);
                                                                
                                                                

                                                                // declaration is written on every scope.
                                                                // printf("int %s = %s;", $2,$4);
                                                                
                                                               /* if (game_step == SETUP ) {
                                                                   printf("int %s = %s; ", $2, $4);
                                                                   $$ = "";
                                                               
                                                               } else {
                                                                char wcond[100];
                                                                   snprintf(wcond, 100,"int %s = %s; ", $2, $4); 
                                                                   $$ = wcond;
                                                               } */

                                                               char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"int %s = %s; ", $2, $4); 
                                                                   $$ = statement;
                                                            }
                                                            } 
    ;
    Assignment: IDENTIFIER '=' OP '.'                   { 
                                                            if( !variable_in_current_scope($1) && !variable_in_global_scope($1) ) {
                                                               char state[50];
                                                                 sprintf(state,"ERROR: variable %s is undefined.", $1);
                                                                 yyerror(state);
                                                            } else {
                                                         

                                                                char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s = %s; ", $1, $3); 
                                                                   $$ = statement
                                                                ;
                                                         

                                                            }
                                                                }
                | IDENTIFIER '=' rect'('COLOR','NUMBER','NUMBER','NUMBER','NUMBER')' '.'               { 
                                                            if( !variable_in_current_scope($1) && !variable_in_global_scope($1) ) {
                                                            
                                                                   char state[50];
                                                                 sprintf(state,"ERROR: variable %s is undefined.", $1);
                                                                 yyerror(state);
                                                            } else {
                                                         

                                                                char *wcond = malloc(500);
                                                                    add_address_to_free_queue(wcond);
                                                                    snprintf(wcond, 500 , "add_block( \"%s\",  %s, %s, %s, %s, 0, 0, 0, \"%s\");" , $1, $7 ,$9 ,$11 ,$13 ,$5);
                                                                $$ = wcond;
                                                         

                                                            }
                                                                }

    ;
    WCOND: COND                                         {;}               
            | IDENTIFIER EV IDENTIFIER                  { if(!identifier_is_block_or_role($1)) {
                                                                        yyerror("ERROR: when must be applied to a block variable");
                                                                     }
                
                                                        char wcond[100];
                                                        snprintf(wcond, 100,"%s (\"%s\", \"%s\")",$2,$1,$3); $$ = wcond;
                                                        }
    ;
    COND: NUMBER Igualdad NUMBER                          {char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s == %s", $1, $3); 
                                                                   $$ = statement;
                                                                }
        | NUMBER Diferente NUMBER                         { char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s != %s", $1, $3); 
                                                                   $$ = statement; }
        | NUMBER '>' NUMBER                               {char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s > %s", $1, $3); 
                                                                   $$ = statement;}
        | NUMBER MayorIgual NUMBER                        {char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s >= %s", $1, $3); 
                                                                   $$ = statement;}
        | NUMBER '<' NUMBER                               {char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s < %s", $1, $3); 
                                                                   $$ = statement;}
        | NUMBER MenorIgual NUMBER                        {char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   snprintf(statement, 100,"%s <= %s", $1, $3); 
                                                                   $$ = statement;}
    ;
    EV: hits                                            {$$ = "hits";}
        | over                                          {$$ = "over";}
        | on                                            {$$ = "over";}
        | under                                         {$$ = "under";}     
        | touches                                       {$$ = "hits";}
    ;
    DIR: up                                             {$$ = "up";}
        | down                                          {$$ = "down";}
        | left                                          {$$ = "left";}
        | right                                         {$$ = "right";}
    ;     
    ATTRIBUTES: speed DIGIT ',' ATR1                    {char aux[100];
                                                         sprintf(aux, "%d , \"%s\" ",$2,$4); 
                                                         $$ = aux;}
        |                                               {$$ = "1 ,\"bounces\" ";}
    ;
    ATR1: bounces                                       {$$ = "bounces";}
        | reset                                         {$$ = "reset";}
    ;
    ;
    OP: OP OPER OP                                      {char aux[100];
                                                         sprintf(aux, "%s %s %s",$1,$2,$3); 
                                                         $$ = aux;}                                   
         | NUMBER                                        {$$ = $1;}       
    OPER:  '+'                                          {$$ = "+";}
        |  '*'                                          {$$ = "*";}
        |  '/'                                          {$$ = "/";}
        |  '-'                                          {$$ = "-";}
    ;
    NUMBER: randomnum '(' DIGIT','DIGIT')'                { char *statement = malloc(100);
                                                               add_address_to_free_queue(statement);
                                                                   sprintf(statement, "int(random(%d,%d))",$3,$5); 
                                                                   $$ = statement;}
        | IDENTIFIER                                    { if(!is_defined_int_variable($1)) {
                                                                yyerror("Bad type convertion, assigning int to a string or vicersa, dunno");
                                                            }
                                                         $$ = $1; }
        | DIGIT                                       { char *statement = malloc(100);
                                                          add_address_to_free_queue(statement);
                                                          sprintf(statement, "%d",$1); 
                                                          $$ = statement;}
    ;
    //ID: IDENTIFIER {;}
    //;
     ID: ID ',' IDENTIFIER                               { char aux[500];
                                                         sprintf(aux, "%s,%s",$1,$3); 
                                                         $$ = aux;}
         | IDENTIFIER                                    { $$ = $1 ;}
     ;

     //give bloque1,bloque2,bloque3 role enemies

    COLOR: yellow                                           {$$ = "YELLOW";}
        | red                                           {$$ = "RED";}
        | blue                                          {$$ = "BLUE";}
        | black                                           {$$ = "BLACK";}
        | grey                                          {$$ = "GREY";}
        | white                                           {$$ = "WHITE";}
        | orange                                          {$$ = "ORANGE";}
        | pink                                           {$$ = "PINK";}
        | purple                                          {$$ = "PURPLE";}
        | light_blue                                           {$$ = "LIGHT_BLUE";}
        | green                                          {$$ = "GREEN";}
    ;
    Instruction:  Link IDENTIFIER TO mouse '.'          { char aux[100];
                                                         sprintf(aux, "link(%s); \n",$2); 
                                                         $$ = aux; }
        |  give ID role IDENTIFIER  '.'                 {char aux[100];
                                                         sprintf(aux, "give_role(%s, %s); \n",$2, $4); 
                                                         $$ = aux;}
        |  IF '(' COND ')' '{'Fbody'}'                  {char aux[4096];
                                                         sprintf(aux, "if(%s){ %s }; \n",$3, $6); 
                                                         $$ = aux;}
        |  win '.'                                      {$$ = "win(); \n";}
        |  lose '.'                                     {$$ = "lose(); \n";}
    ;
    ///
%%


/* ----------------  C code  --------------------------------*/ 

int main (void) {
    // set scope to global
    processing_current_scope = GLOBAL_SCOPE;
    
    current_scope_variables = NULL;
    global_scope_variables = NULL;
    setup_links = NULL;
    setup_moves = NULL;
    defined_roles = NULL;
    functions_called = NULL;
    functions_defined = NULL;
    setup_blocks = NULL;
    pending_frees = NULL;
    setup_links_wasd = NULL;

    game_step = SETUP;

    return yyparse ( );
}

int variable_in_current_scope(char *identifier) {
    if( processing_current_scope == GLOBAL_SCOPE ) {
        return variable_in_global_scope(identifier);
    }
    
    struct  variables* the_variables = current_scope_variables;

    int found = 0;
    while( the_variables != 0 ) {
        if (!strcmp(identifier, the_variables->identifier) ){
            found = 1;
            break;
        }
        the_variables = the_variables->next;
    }

    return found;
}

int variable_in_global_scope(char *identifier) {
    struct  variables* the_variables = global_scope_variables;

    int found = 0;
    while( the_variables != 0 ) {
        if (!strcmp(identifier, the_variables->identifier) ) {
            found = 1;
            break;
        }
        the_variables = the_variables->next;
    }

    return found;
}
    

    int add_variable_to_current_scope(char *identifier, enum variable_types type) {
    struct  variables* the_variables = current_scope_variables;

    // printf("about to add %s", identifier);
    
    // alloc space for new variable. 
    if( the_variables == NULL) {


        /* if( processing_current_scope == GLOBAL_SCOPE ) {
            
            global_scope_variables = malloc(sizeof(struct variables));
            
            if( global_scope_variables == NULL ){
            return -1;
            }
            
            the_variables = global_scope_variables;

        } else { */

            current_scope_variables = malloc(sizeof(struct variables));

            if( current_scope_variables == NULL ){
            return -1;
            }

            the_variables = current_scope_variables;
        /* } */


        
    } else {
        //no estaba en NULL the_variables

        while( the_variables->next != NULL ){
            the_variables = the_variables->next;	
        }
        //me deja el next en NULL

        the_variables->next = malloc(sizeof(struct variables));

        if( the_variables->next == NULL ){
            return -1;
        }

        the_variables = the_variables->next;
    }

        //ya tengo lugar para dejar mi variable
        

        the_variables->identifier = malloc(strlen(identifier));

        if( the_variables->identifier == NULL ){
            return -1;	
        }
            
        // set variable identifier
        memcpy(the_variables->identifier, identifier, strlen(identifier));
        

        the_variables->variable_type = type;
        
        
    }

    int identifier_is_block_or_role(char *identifier) {
        struct  variables* the_variables =  current_scope_variables;
        
        int found = 0;
        struct identifier_list_node *roles = defined_roles;
        while( roles != NULL ) {
            if (!strcmp(identifier, roles->identifier) ){
                found = 1;
                break;
            }
            roles = roles->next;
        }
        
        if( found ) {
            return found;
        }
        
        
        found = 0;
        while( the_variables != NULL ) {
            if (!strcmp(identifier, the_variables->identifier) ){
                found = 1;
                break;
            }
        the_variables = the_variables->next;
        }

        // printf("found is %d for variable %s\n", found, identifier);

        if( found ) {
            if(the_variables->variable_type == BLOCK) {
                    return 1;
            }
        }

        found = 0;
        the_variables = processing_current_scope == GLOBAL_SCOPE ? NULL : global_scope_variables;
        
        while( !found && the_variables != NULL ) {
            if (!strcmp(identifier, the_variables->identifier) ){
                found = 1;
                break;
            }
        the_variables = the_variables->next;
        }

        if( found ) {
            if (the_variables->variable_type == BLOCK ) {
                return 1;
            }
        }
        
        return 0;
    }

    int add_role_if_not_exists(char *identifier) {
        if( defined_roles == NULL ){
            defined_roles = malloc(sizeof(struct identifier_list_node));
            if( defined_roles == NULL ){
                return -1;
            }
            memset(defined_roles, 0, sizeof(struct identifier_list_node));
            strcat(defined_roles->identifier, identifier);
        }
        else {
            struct identifier_list_node * new_role = defined_roles;
            while(new_role->next != NULL) {
                new_role = new_role->next;
            }
            new_role->next = malloc(sizeof(struct identifier_list_node));
            if( new_role->next == NULL ){
                return -1;
            }
            memset(new_role->next, 0, sizeof(struct identifier_list_node));
            strcat(new_role->next->identifier, identifier);

        }
        return 0;
    }



    void add_function_declaration(char* fname){
        
        if(functions_defined == NULL ){
            //tengo que crear functions_defined.
            functions_defined = malloc(sizeof(struct identifier_list_node));
            if( functions_defined == NULL ){
            yyerror("malloc error.");
                return ;
            }
            memset(functions_defined, 0, sizeof(struct identifier_list_node));

            strcpy(functions_defined->identifier, fname);
      

    } else {
            struct identifier_list_node * function = functions_defined;
            while(function->next != NULL ) {
                function = function->next;
            }
            function->next = malloc(sizeof(struct identifier_list_node));
            if( function->next == NULL ){
                yyerror("malloc error.");
                return ;
            }

            memset(function->next, 0, sizeof(struct identifier_list_node));

            strcpy(function->next->identifier, fname);
            
    }

}

 
    void add_function_calls(char* fname){
        

        if(functions_called == NULL ){
            //tengo que crear functions_defined.
            functions_called = malloc(sizeof(struct identifier_list_node));
            if( functions_called == NULL ){
            yyerror("malloc error.");
                return ;
            }
            memset(functions_called, 0, sizeof(struct identifier_list_node));

            strcpy(functions_called->identifier, fname);
      

        } else {
            struct identifier_list_node * function = functions_called;
            while(function->next != NULL ) {
                function = function->next;
            }
            function->next = malloc(sizeof(struct identifier_list_node));
            if( function->next == NULL ){
                yyerror("malloc error.");
                return ;
            }

            memset(function->next, 0, sizeof(struct identifier_list_node));

            strcpy(function->next->identifier, fname );
        
        }

    }
    

    int function_already_defined(char* fname) {
        struct  identifier_list_node* function =  functions_defined;
        
        int found = 0;
        while( function != NULL ) {
            if (!strcmp(fname, function->identifier) ){
                found = 1;
                break;
            }
            function = function->next;
        }
        
        return found;
    }

    int verify_functions_called() {
        
        // cada function de function_called debe estar en functions_defined
        struct  identifier_list_node * function =  functions_called;  

        while( function != NULL) {

            //aca tengo una function_called
            //vemos si esa funtion_called coincide con alguna de las functions_defined
            int found = 0;
            struct  identifier_list_node * function2 =  functions_defined; 
            
            while( function2 != NULL ){

                if (!strcmp(function->identifier, function2->identifier) ){
                    found = 1;
                    break;
                }
                function2 = function2->next;
            }

            if(found == 0)
                return 0;
        
            function = function->next;
        }

        //salio todo bien: todas fueron encontradas correctamente!
        return 1;
    }

    int is_defined_int_variable(char *identifier) {
        struct  variables* the_variables =  current_scope_variables;
        
        
        int found = 0;
        while( the_variables != NULL ) {
            if (!strcmp(identifier, the_variables->identifier) ){
                found = 1;
                break;
            }
        the_variables = the_variables->next;
        }

        if( found ) {
            if(the_variables->variable_type == INTEGER) {
                    return 1;
            }
        }

        found = 0;
        the_variables = processing_current_scope == GLOBAL_SCOPE ? NULL : global_scope_variables;
        
        while( !found && the_variables != NULL ) {
            if (!strcmp(identifier, the_variables->identifier) ){
                found = 1;
                break;
            }
        the_variables = the_variables->next;
        }

        if( found ) {
            if (the_variables->variable_type == INTEGER ) {
                return 1;
            }
        }
        
        return 0;
    }

    int add_address_to_free_queue(char *address) {
        if(pending_frees == NULL ){
            //tengo que crear functions_defined.
            pending_frees = malloc(sizeof(struct addresses_list));
            if( pending_frees == NULL ){
            yyerror("malloc error.");
                return 1;
            }
            memset(pending_frees, 0, sizeof(struct addresses_list));

            pending_frees->address = address;
      

        } else {
            struct addresses_list * addr = pending_frees;
            while(addr->next != NULL ) {
                addr = addr->next;
            }
            addr->next = malloc(sizeof(struct addresses_list));
            if( addr->next == NULL ){
                yyerror("malloc error.");
                return 1;
            }

            memset(addr->next, 0, sizeof(struct addresses_list));

            addr->next->address = address;
        
        }

        return 0;
    }

    void free_pendings() {
        while(pending_frees != NULL) {
            free(pending_frees->address);
            struct addresses_list *temp = pending_frees;
            pending_frees = pending_frees->next;
            free(temp);
        }
    }

    void free_setup_data() {
        while(setup_links != NULL) {
            struct identifier_list_node *temp = setup_links;
            setup_links = setup_links->next;
            free(temp);
        }

        while(setup_moves != NULL) {
            struct move_list_node *temp = setup_moves;
            setup_moves = setup_moves->next;
            free(temp);
        }

        while(defined_roles != NULL) {
            struct identifier_list_node *temp = defined_roles;
            defined_roles = defined_roles->next;
            free(temp);
        }


        while(setup_links_wasd != NULL) {
            struct identifier_list_node *temp = setup_links_wasd;
            setup_links_wasd = setup_links_wasd->next;
            free(temp);
        }

        while(setup_blocks != NULL) {
            struct block_variable *temp = setup_blocks;
            setup_blocks = setup_blocks->next;
            free(temp);
        }
    }

    void free_function_structures(){
        while(functions_defined != NULL) {
            struct identifier_list_node *temp = functions_defined;
            functions_defined = functions_defined->next;
            free(temp);
        }

        while(functions_called != NULL) {
            struct identifier_list_node *temp = functions_called;
            functions_called = functions_called->next;
            free(temp);
        }


    }



void yyerror (char *s) {
    
    fprintf (stderr, "%s\n", s);
    
    }




/* ---------------- C code  --------------------------------*/ 
