//Setup 
import java.util.Map;
int user_wide = 320;
int user_height = 230;
int screen_width = 640;
int screen_height = 460;
int flag = 1;


//direction { 0: STATIC , 1:UP, 2:DOWN, 3:RIGHT, 4:LEFT }
////effect { 0: STATIC , 1:BOUNCE, 2:RESET }
//  0  1     2          3          4      5         6             7               8     9     
//  x, y, initial_x, initial_y, size_x, size_y, direction, current_direction, effect, speed
///////////////

HashMap<String, PShape> bloques = new HashMap<String, PShape> ();
HashMap<String, ArrayList<Integer>> bloques_data = new HashMap<String, ArrayList<Integer>> ();


///////////////

//// Rect
//PShape id_rect;
//IntDict id_rect_specs;
//String id_rect_string = "id_rect";

//// Player 
//PShape player;
//IntDict player_specs;
//String playeer_string = "player";


////String bloques[] = new String[100];
  
//ArrayList<PShape> shapes = new ArrayList();
////int cant_bloques=1;



void setup() {
  size(640, 460);
}



void draw() {

  background(0);
  
  fill(235,134,10);
  ellipse(mouseX, mouseY, 20, 20);
  
 
if(flag == 1){
  add_block("block_name2" , 10,  100,  10, 10, 1 ,  1, 2) ;
  add_block("block_name4" , 100,  40,  20, 75, 1 ,  1, 1) ;
  move("block_name2", "up", 5, "bounce");
  move("block_name4", "down", 2, "bounce");
  flag=0;
}
 for (Map.Entry<String, ArrayList<Integer>> me : bloques_data.entrySet()) {
    check_movement(me.getValue());
    translate(me.getValue().get(0), me.getValue().get(1));
    shape(bloques.get(me.getKey()));   
  }

  //shape(id_rect);
  //link("id_rect");
}

void
move(String dic1, String direction, int speed_v, String effect){  //Setter
   bloques_data.get(dic1).set(9,speed_v);
  switch(direction){
     case "down":
               bloques_data.get(dic1).set(7,2);
               
     break;
      case "up":
               bloques_data.get(dic1).set(7,1);
     break;
      case "right":
               bloques_data.get(dic1).set(7,3);
     break;
      case "left":
               bloques_data.get(dic1).set(7,4);
     break;
  }  
   switch(effect){
     case "bounces":
                   bloques_data.get(dic1).set(8,1);
     break;
      case "reset":
               bloques_data.get(dic1).set(8,2);
     break;
      case "static":
                bloques_data.get(dic1).set(8,0);
     break;
  }  
     //print(bloques_data.get(dic1));
}


//  0  1     2          3          4      5         6             7               8     9     
//  x, y, initial_x, initial_y, size_x, size_y, direction, current_direction, effect, speed
void check_movement( ArrayList<Integer> data_array ){
   switch(data_array.get(8)){ /// Chequeo efecto 
     case 0:
             //Static, no hago nada
     break;
      case 1: // Bounce, chequeo current_direction y bounceo-> cambio current_direction 
             switch(data_array.get(7)){  // chequeo direccion
               case 1:  //esta yendo para arriba 
                 if( data_array.get(1)  <=  -data_array.get(3) ){ //data_array.get(3)
                 data_array.set(7, 2);         
                 data_array.set(1, data_array.get(1) + data_array.get(9));
                 }
                 else{  data_array.set(1, data_array.get(1) - data_array.get(9));} //
               break;
               case 2:  //esta yendo para arriba 
                 if( data_array.get(1)  >=  screen_height - data_array.get(3)- data_array.get(5)){ // Si llega arriba de la pantalla   + id_rect_specs.get("id_rect_size_x")
                 data_array.set(7, 1);         
                 data_array.set(1, data_array.get(1) - data_array.get(9));
                 }
                 else{data_array.set(1, data_array.get(1) + data_array.get(9));} //
               break;
             }
     }
}
     //          case 3: // is going RIGHT
     //          if( id_rect_specs.get("id_rect_x")  + id_rect_specs.get("id_rect_size_x") >= screen_width - id_rect_specs.get("id_rect_initial_x")){ // Si llega al final de la derecha de la pantalla   + id_rect_specs.get("id_rect_size_x")
     //            id_rect_specs.set("id_rect_current_direction", 4); // cambio que vaya para alla <-
     //            id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_x")- id_rect_specs.get("id_rect_speed")); //
     //            }
     //            else{id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_x") + id_rect_specs.get("id_rect_speed"));} //
     //         break;
     //         case 4: // ig going left
     //            if( id_rect_specs.get("id_rect_x")  <= 0 - id_rect_specs.get("id_rect_initial_x") ){ // Si llega al final de la izquierda de la pantalla         + id_rect_specs.get("id_rect_size_x")
     //            id_rect_specs.set("id_rect_current_direction", 3); // cambio que vaya para alla ->
     //            id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_x")+ id_rect_specs.get("id_rect_speed")); //
                 
     //            }
     //            else {id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_x") - id_rect_specs.get("id_rect_speed")); }//
     //         break;
     //        }
             
              
     //break;
     // case 2:
     //  switch(id_rect_specs.get("id_rect_current_direction")){
     //          case 1:  //esta yendo para arriba 
     //            if( id_rect_specs.get("id_rect_y")  <= -id_rect_specs.get("id_rect_initial_y")){ // Si llega arriba de la pantalla   + id_rect_specs.get("id_rect_size_x")
     //            id_rect_specs.set("id_rect_y",id_rect_specs.get("id_rect_initial_y")); //
     //            }
     //            else{id_rect_specs.set("id_rect_y", id_rect_specs.get("id_rect_y") - id_rect_specs.get("id_rect_speed"));} //
     //          break;
     //           case 2:// esta yendo para abajo
     //            if( id_rect_specs.get("id_rect_y")  >= screen_height -id_rect_specs.get("id_rect_initial_y") - id_rect_specs.get("id_rect_size_y")){ // Si llega abajo de la pantalla         + id_rect_specs.get("id_rect_size_x")
     //              id_rect_specs.set("id_rect_y",id_rect_specs.get("id_rect_initial_y")); //
     //            }
     //            else {id_rect_specs.set("id_rect_y", id_rect_specs.get("id_rect_y") + id_rect_specs.get("id_rect_speed")); }//
     //          break;
     //          case 3: // is going RIGHT
     //            if( id_rect_specs.get("id_rect_x")  + id_rect_specs.get("id_rect_size_x") >= screen_width - id_rect_specs.get("id_rect_initial_x")){ // Si llega al final de la derecha de la pantalla   + id_rect_specs.get("id_rect_size_x")
     //              id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_initial_x")); //
     //            }
     //            else{id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_x") + id_rect_specs.get("id_rect_speed"));} //
     //         break;
     //          case 4: // ig going left
     //            if( id_rect_specs.get("id_rect_x")  <= 0 - id_rect_specs.get("id_rect_initial_x") ){ // Si llega al final de la izquierda de la pantalla         + id_rect_specs.get("id_rect_size_x")
     //            id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_initial_x")); //
     //            //id_rect_specs.set("id_rect_current_x", id_rect_specs.get("id_rect_x")+ id_rect_specs.get("id_rect_speed"));
     //            }
     //            else {id_rect_specs.set("id_rect_x", id_rect_specs.get("id_rect_x") - id_rect_specs.get("id_rect_speed")); }//
     //         break;
     //     }  
     //}
//}

//void link (String obj){
//   id_rect_specs.set( obj + "_x", mouseX);
//   id_rect_specs.set(obj +"_y", mouseY);
//}

void add_block(String block_name ,int id_rect_x, int id_rect_y, int id_rect_size_x,
              int id_rect_size_y, int id_rect_direction, int id_rect_effect, int id_rect_speed)  {
     PShape a = new PShape();
     a = createShape(RECT, id_rect_x,id_rect_y, id_rect_size_x, id_rect_size_y);
     ArrayList<Integer> ar = new ArrayList();
     ar.add(id_rect_x);   ar.add(id_rect_y);  ar.add(id_rect_x);   ar.add(id_rect_y); ar.add(id_rect_size_x); 
     ar.add(id_rect_size_y);   ar.add(id_rect_direction);   ar.add(id_rect_direction); ar.add(id_rect_effect);   ar.add(id_rect_speed);
     bloques.put(block_name, a);
     bloques_data.put(block_name, ar);
}











