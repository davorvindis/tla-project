//if(y_rect1 <= 0   && dir_rect1 == 1){
//  dir_rect1 = 0;
//}


int speed;

int x_flappy = 0;
int y_flappy = 0;
int r_flappy = 255;
int g_flappy = 255;
int b_flappy = 0;
int x_size_flappy = 60;
int y_size_flappy = 60;

int grid_x = 50 * 20;
int grid_Y = 50 * 20;

int x_initial_col = 640;
int y_initial_col = 0;
int x_col = 640;
int y_col = 0;
int r_green = 0;
int g_green = 255;
int b_green = 0;

float random_1 = random(20, 400); 
int x_size_initial_col = 60;
int y_size_initial_col = floor(random_1);
int x_size_col = x_size_initial_col;
int y_size_col = y_size_initial_col;

int x_initial_col2 = 640;
int y_initial_col2 = y_initial_col + y_size_col + 100;
int x_col2 = x_initial_col2;
int y_col2 = x_initial_col2;

int x_size_initial_col2 = 60;
int y_size_initial_col2 = 430 - y_col2;
int x_size_col2 = x_size_initial_col2;
int y_size_col2 = y_size_initial_col2;

int speed_col = 5;
int dir_x_col = -1;

int speed_col2 = 5;
int dir_x_col2 = -1;

void setup() {
  size(640, 430);
  //noStroke();
  
  set_variables();
   
}


void draw() {
  background(51);
  
  // flappy bird
  fill(r_flappy, g_flappy, b_flappy);
  rect(x_flappy, y_flappy, x_size_flappy, y_size_flappy);
  
  // col
  fill(r_green, g_green, b_green);
  rect(x_col, y_col, x_size_col, y_size_col);
  
  // col 2
  fill(r_green, g_green, b_green);
  rect(x_col2, y_col2, x_size_col2, y_size_col2);

  x_flappy = mouseX - x_size_flappy/2;
  y_flappy = mouseY - y_size_flappy/2;
  
  x_col = x_col + (speed_col * dir_x_col);
  x_col2 = x_col2 + (speed_col2 * dir_x_col2);
  
  if ( (x_flappy < x_col && x_flappy + x_size_flappy/2 >= x_col) || (x_flappy < x_col + x_size_col && x_flappy + x_size_flappy/2 >= x_col  ) ){
   if ((y_flappy < y_col && y_flappy + y_size_flappy/2 >= y_col) || (y_flappy < y_col + y_size_col && y_flappy + y_size_flappy/2 >= y_col ) ){
        background(255);
        delay(1500);
        setup();
    }
  }
  
  if ( (x_flappy < x_col2 && x_flappy + x_size_flappy/2 >= x_col2) || (x_flappy < x_col2 + x_size_col2 && x_flappy + x_size_flappy/2 >= x_col2  ) ){
   if ((y_flappy < y_col2 && y_flappy + y_size_flappy/2 >= y_col2 ) || (y_flappy < y_col2 + y_size_col2 && y_flappy + y_size_flappy/2 >= y_col2 ) ){
        background(255);
        delay(1500);
        setup();
    }
  }
  
  if( x_col <= 0 ) {
    col_reached_end();
  }
  
  if( x_col2 <= 0 ) {
    col2_reached_end();
  }
  
 
}

void col_reached_end() {
  
  x_col = x_initial_col;
  y_col = y_initial_col;
  
  x_size_col = x_size_initial_col;
  float random_1 = random(20, 400); 
  int y_size_initial_col = floor(random_1);
  y_size_col = y_size_initial_col;
  
}

void col2_reached_end() {
     speed_col2 = 5;
     dir_x_col2 = 1;

  
    //y_initial_col2 = y_initial_col + y_size_col + 100;
    //x_col2 = x_initial_col2;
    //y_col2 = y_initial_col2;

    //x_size_initial_col2 = 60;
    //y_size_initial_col2 = 430 - y_col2;
    //x_size_col2 = x_size_initial_col2;
    //y_size_col2 = y_size_initial_col2;
  
}


void set_variables() {

 x_flappy = 0;
  y_flappy = 0;
  r_flappy = 255;
  g_flappy = 255;
  b_flappy = 0;
  x_size_flappy = 60;
  y_size_flappy = 60;


  x_initial_col = 640;
  y_initial_col = 0;
  x_col = 640;
   y_col = 0;
   r_green = 0;
  g_green = 255;
   b_green = 0;
   x_size_col = 60;
  random_1 = random(20, 400); 
   y_size_col = floor(random_1);

  speed_col = 5;
  dir_x_col = -1;
  
  
  x_col2 = x_initial_col2;
  y_col2 = y_initial_col2;
  x_size_col2 = x_size_initial_col2;
  y_size_col2 = y_size_initial_col2;
}
