
%option noyywrap
%x comentario
%{

    #include "y.tab.h"
    void yyerror (char *s);
    int yylex();

    
    #include <stdlib.h>
    #include <stdio.h>
%}
    
 
%%
"setup"                     return setup;
"game"                      return game;
"end"                       return end;
"Block"                     return Block;
"move"                      return move;
"int"                       return Integer;
"when"                      return when;
"Position"                  return Position;
"link"                      return Link;
"->"                        return flechita;
"rect"                      return rect;
"role"                      return role;
"random"                    return randomnum;
"mouse"                     return mouse;
"WASD"                      return flechas_direccion;
"if"                        return IF;
"to"                        return TO;
"give"                      return give;
"lose"                      return lose;
"win"                       return win;
"gameover"                  return gameover;

"hits"                      return hits;
"over"                      return over;
"on"                        return on; 
"under"                     return under;
"touches"                   {return touches;}
[0-9]+                      {yylval.num = atoi(yytext); return DIGIT;} 






"right"                     return right;
"up"                        return up;
"left"                      return left;
"down"                      return down;


"blue"                      return blue;
"red"                       return red;
"yellow"                    return yellow;
"black"                      return black;
"grey"                       return grey;
"white"                    return white;
"orange"                      return orange;
"pink"                       return pink;
"purple"                    return purple;
"light_blue"                      return light_blue;
"green"                       return green;



"speed"                     return speed;
"bounces"                   return bounces;
"reset"                     return reset;  
"=="                        return Igualdad;
"!="                        return Diferente;
"<="                        return MenorIgual;
">="                        return MayorIgual;
[A-Za-z][_A-Za-z0-9]*       {   yylval.id = strdup(yytext);
                                yylval.id[yyleng] = 0;
                                return IDENTIFIER;}
[-+=*/;(){},.:]                 {return yytext[0];}

%%







