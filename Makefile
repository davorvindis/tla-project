all:
	lex game.l
	yacc -d game.y
	gcc -g y.tab.c lex.yy.c -ly -o compile


clean: 
	rm ./compile ./lex.yy.c ./y.tab.*
