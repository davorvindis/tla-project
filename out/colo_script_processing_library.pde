//Setup 
import java.util.Map;
int user_wide = 320;
int user_height = 230;
int screen_width = 640;
int screen_height = 460;
int flag = 1;
boolean win = false;
boolean lose = false;
PFont f;

//// Colors
 HashMap <String, Integer > colores = new HashMap<String , Integer> ();


//direction { 0: STATIC , 1:UP, 2:DOWN, 3:RIGHT, 4:LEFT }
////effect { 0: STATIC , 1:BOUNCE, 2:RESET }
//  0  1     2          3          4      5         6             7               8     9     
//  x, y, initial_x, initial_y, size_x, size_y, direction, current_direction, effect, speed , r ,g, b
///////////////

HashMap<String, ArrayList<Integer>> bloques_data = new HashMap<String, ArrayList<Integer>> ();



void
move(String dic1, String direction, int speed_v, String effect){  //Setter
if((mapa_de_roles.get(dic1) != null)){
  for(int i = 0 ; i < mapa_de_roles.get(dic1).size() ; i++){
    move(  mapa_de_roles.get(dic1).get(i), direction, speed_v, effect);
  }
  return;
}
   bloques_data.get(dic1).set(9,speed_v);
  switch(direction){
     case "down":
               bloques_data.get(dic1).set(7,2);         
     break;
      case "up":
               bloques_data.get(dic1).set(7,1);
     break;
      case "right":
               bloques_data.get(dic1).set(7,3);
     break;
      case "left":
               bloques_data.get(dic1).set(7,4);
     break;
  }  
   switch(effect){
     case "bounces":
                   bloques_data.get(dic1).set(8,1);
     break;
      case "reset":
               bloques_data.get(dic1).set(8,2);
     break;
      case "static":
                bloques_data.get(dic1).set(8,0);
     break;
  }  
}


//  0  1     2          3          4      5         6             7               8     9    10  11  12 
//  x, y, initial_x, initial_y, size_x, size_y, direction, current_direction, effect, speed, r,  g,   b 
void check_movement( ArrayList<Integer> data_array ){
   switch(data_array.get(8)){ /// Chequeo efecto 
     case 0:
             //Static, no hago nada
     break;
      case 1: // Bounce, chequeo current_direction y bounceo-> cambio current_direction 
             switch(data_array.get(7)){  // chequeo direccion
               case 1:  //esta yendo para arriba 
                 if( data_array.get(1)  <=  0 ){ // si llega al tope
                 data_array.set(7, 2);// cambio que vaya para abajo
                 data_array.set(1, data_array.get(1) + data_array.get(9));// avanzo 
                 }
                 else{  data_array.set(1, data_array.get(1) - data_array.get(9));} //avanzo
               break;
               case 2:  //esta yendo para abajo
              
                 if( data_array.get(1)  >= screen_height -  data_array.get(5) ){ 
                 data_array.set(7, 1);         
                 data_array.set(1, data_array.get(1) - data_array.get(9));
                 }
                 else{data_array.set(1, data_array.get(1) + data_array.get(9));} //
               break;
              case 3: // is going RIGHT
               if(data_array.get(0)  >= screen_width -  data_array.get(4)){ // Si llega al final de la derecha de la pantalla   + id_rect_specs.get("id_rect_size_x")
                 data_array.set(7, 4);         
                 data_array.set(0, data_array.get(0) - data_array.get(9));
                 }
                 else{data_array.set(0, data_array.get(0) + data_array.get(9));} 
              break;
              case 4: // is going LEFT
               if(data_array.get(0)  <= 0 ){ // Si llega al final de la derecha de la pantalla   + id_rect_specs.get("id_rect_size_x")
                 data_array.set(7, 3);         
                 data_array.set(0, data_array.get(0) + data_array.get(9));
                 }
                 else{data_array.set(0, data_array.get(0) - data_array.get(9));} 
              break;
             }
       break;
       case 2: // Reset
          switch(data_array.get(7)){  // chequeo direccion
               case 1:  //esta yendo para arriba 
                 if( data_array.get(1)  <=  0 ){ // si llega al tope
                    data_array.set(1, data_array.get(3));         
                 }
                 else{data_array.set(1, data_array.get(1) - data_array.get(9));} //avanzo
               break;
               case 2:  //esta yendo para abajo
                 if( data_array.get(1)  >= screen_height -  data_array.get(5) ){  
                   data_array.set(1, data_array.get(3));   
                 }
                 else{data_array.set(1, data_array.get(1) + data_array.get(9));} //
               break;
              case 3: // is going RIGHT
               if(data_array.get(0)  >= screen_width -  data_array.get(4)){ // Si llega al final de la derecha de la pantalla   + id_rect_specs.get("id_rect_size_x")
                 data_array.set(0, data_array.get(2));  
                 }
                 else{data_array.set(0, data_array.get(0) + data_array.get(9));
               } 
              break;
              case 4: // is going LEFT
               if(data_array.get(0)  <= 0 ){ //
                 data_array.set(0, data_array.get(2) );
                 }
                 else{data_array.set(0, data_array.get(0) - data_array.get(9));} 
              break;
             }
      break;
     }
}
   
void link (String obj){
   ArrayList<Integer> ar = bloques_data.get(obj);
   ar.set(0, mouseX - ar.get(4)/2);
   ar.set(1, mouseY - ar.get(5)/2);
}

//  0  1     2          3          4      5         6             7               8     9     
//  x, y, initial_x, initial_y, size_x, size_y, direction, current_direction, effect, speed , r ,g, b
///////////////

void add_block(String block_name ,int id_rect_x, int id_rect_y, int id_rect_size_x,
              int id_rect_size_y, int id_rect_direction, int id_rect_effect, int id_rect_speed, String Color)  {
    if(bloques_data.get(block_name) != null){
      ArrayList<Integer> ar = bloques_data.get(block_name);
      ar.set(0,id_rect_x); 
      ar.set(1,id_rect_y); 
      ar.set(2,id_rect_x); 
      ar.set(3,id_rect_y); 
      ar.set(4,id_rect_size_x); 
      ar.set(5,id_rect_size_y); 
      ar.set(6,id_rect_direction);
      ar.set(7,id_rect_direction);
      ar.set(8,id_rect_effect);
      ar.set(9,id_rect_speed);
        switch(Color){
       case "YELLOW":
            ar.set(10,255); ar.set(11,255); ar.set(12,0);
        break;
       case "BLUE":
            ar.set(10,0); ar.set(11,0); ar.set(12,255);
        break;
       case "BLACK":
            ar.set(10,0); ar.set(11,0); ar.set(12,0);
        break;  
        case "GREY":
            ar.set(10,192); ar.set(11,192); ar.set(12,192);
        break;
        case "WHITE":
            ar.set(10,255); ar.set(11,255); ar.set(12,255);
        break;
        case "RED":
            ar.set(10,255); ar.set(11,0); ar.set(12,0);
        break;
        case "ORANGE":
            ar.set(10,255); ar.set(11,128); ar.set(12,0);
        break;
         case "PURPLE":
            ar.set(10,153); ar.set(11,0); ar.set(12,153);
        break;
         case "PINK":
            ar.set(10,255); ar.set(11,0); ar.set(12,127);
        break;
         case "GREEN":
            ar.set(10,0); ar.set(11,255); ar.set(12,0);
        break;
         case "LIGHT_BLUE":
            ar.set(10,102); ar.set(11,255); ar.set(12,255);
        break;
     }
         return;
      }    
    
     ArrayList<Integer> ar = new ArrayList();
     ar.add(id_rect_x);   ar.add(id_rect_y);  ar.add(id_rect_x);   ar.add(id_rect_y); ar.add(id_rect_size_x); 
     ar.add(id_rect_size_y);   ar.add(id_rect_direction);   ar.add(id_rect_direction); ar.add(id_rect_effect);   ar.add(id_rect_speed);
        switch(Color){
       case "YELLOW":
            ar.add(255); ar.add(255); ar.add(0);
        break;
       case "BLUE":
            ar.add(0); ar.add(0); ar.add(255);
        break;
       case "BLACK":
            ar.add(0); ar.add(0); ar.add(0);
        break;  
        case "GREY":
            ar.add(192); ar.add(192); ar.add(192);
        break;
        case "WHITE":
            ar.add(255); ar.add(255); ar.add(255);
        break;
        case "RED":
            ar.add(255); ar.add(0); ar.add(0);
        break;
        case "ORANGE":
            ar.add(255); ar.add(128); ar.add(0);
        break;
         case "PURPLE":
            ar.add(153); ar.add(0); ar.add(153);
        break;
         case "PINK":
            ar.add(255); ar.add(0); ar.add(127);
        break;
         case "GREEN":
            ar.add(0); ar.add(255); ar.add(0);
        break;
         case "LIGHT_BLUE":
            ar.add(102); ar.add(255); ar.add(255);
        break;
        }    
     bloques_data.put(block_name, ar);
}


boolean hits ( String dic1, String obj2){
  if((mapa_de_roles.get(obj2) != null)){
    for(int i = 0 ; i < mapa_de_roles.get(obj2).size(); i++){
      if( hits( dic1, mapa_de_roles.get(obj2).get(i))){
        return true;
      }
    }
    return false;
  }
       ArrayList<Integer> ar1 = bloques_data.get(dic1);
       ArrayList<Integer> ar2 = bloques_data.get(obj2);
       int x_1 = ar1.get(0);
       int y_1 = ar1.get(1);
       int x_2 = ar2.get(0);
       int y_2 = ar2.get(1);
       if( x_1 + ar1.get(4)>= x_2  && x_1 <= x_2 + ar2.get(4)){
         if( y_1 + +ar1.get(5) >= y_2  && y_1 <= y_2 +ar2.get(5)){
           return true;
         }
       }
      return false;
}

boolean under ( String dic1, String obj2){
  if((mapa_de_roles.get(obj2) != null)){
    for(int i = 0 ; i < mapa_de_roles.get(obj2).size(); i++){
      if( hits( dic1, mapa_de_roles.get(obj2).get(i))){
        return true;
      }
    }
    return false;
  }
       ArrayList<Integer> ar1 = bloques_data.get(dic1);
       ArrayList<Integer> ar2 = bloques_data.get(obj2);
       int x_1 = ar1.get(0);
       int y_1 = ar1.get(1);
       int x_2 = ar2.get(0);
       int y_2 = ar2.get(1);
         if( y_1  >= y_2 +ar2.get(5)){
           return true;
         }
      return false;
}


boolean over ( String dic1, String obj2){
  if((mapa_de_roles.get(obj2) != null)){
    for(int i = 0 ; i < mapa_de_roles.get(obj2).size(); i++){
      if( hits( dic1, mapa_de_roles.get(obj2).get(i))){
        return true;
      }
    }
    return false;
  }
       ArrayList<Integer> ar1 = bloques_data.get(dic1);
       ArrayList<Integer> ar2 = bloques_data.get(obj2);
       int x_1 = ar1.get(0);
       int y_1 = ar1.get(1);
       int x_2 = ar2.get(0);
       int y_2 = ar2.get(1);
         if( y_1 + ar1.get(5)  <= y_2 ){
           return true;
         }
      return false;
}


HashMap<String, ArrayList<String>> mapa_de_roles = new HashMap<String, ArrayList<String>>();
ArrayList<String> bloques_con_rol = new ArrayList();

void give_role (String obj,  String rol ){
  String[] q = splitTokens(obj, ","); 
  if(mapa_de_roles.get(rol) == null){
    ArrayList<String> aux = new ArrayList();
   mapa_de_roles.put(rol, aux); 
  }
  for(int i = 0 ; i< q.length ; i++){
    if (bloques_data.get(q[i]) != null){
     mapa_de_roles.get(rol).add(q[i]);
    }
  }
}


void win(){
  win = true;
  setup();
}
void lose(){
    lose=true;
    setup();
}


void change_color (String dic1, String obj2){
   if((mapa_de_roles.get(dic1) != null)){
    for(int i = 0 ; i < mapa_de_roles.get(dic1).size(); i++){
       change_color(mapa_de_roles.get(dic1).get(i), obj2);
    }
    return;
  }
  
   ArrayList<Integer> ar = bloques_data.get(dic1);
   switch(obj2){
       case "YELLOW":
            ar.set(10,255); ar.set(11,255); ar.set(12,0);
        break;
       case "BLUE":
            ar.set(10,0); ar.set(11,0); ar.set(12,255);
        break;
       case "BLACK":
            ar.set(10,0); ar.set(11,0); ar.set(12,0);
        break;  
        case "GREY":
            ar.set(10,192); ar.set(11,192); ar.set(12,192);
        break;
        case "WHITE":
            ar.set(10,255); ar.set(11,255); ar.set(12,255);
        break;
        case "RED":
            ar.set(10,255); ar.set(11,0); ar.set(12,0);
        break;
        case "ORANGE":
            ar.set(10,255); ar.set(11,128); ar.set(12,0);
        break;
         case "PURPLE":
            ar.set(10,153); ar.set(11,0); ar.set(12,153);
        break;
         case "PINK":
            ar.set(10,255); ar.set(11,0); ar.set(12,127);
        break;
         case "GREEN":
            ar.set(10,0); ar.set(11,0); ar.set(12,255);
        break;
         case "LIGHT_BLUE":
            ar.set(10,102); ar.set(11,255); ar.set(12,255);
        break;
     } 
}


HashMap<String, ArrayList<Integer>> keyboard_players = new HashMap<String, ArrayList<Integer>> ();
ArrayList<String> k_p = new ArrayList<String> ();
void link_keyboard(String p){
  k_p.add(p);
}

void check_key_move(){
     if (k_p.size() == 0){return;}
  switch (key){
    case 'w':
          bloques_data.get(k_p.get(0)).set(1,  bloques_data.get(k_p.get(0)).get(1) -3);
    break;   
    case 'a':
          bloques_data.get(k_p.get(0)).set(0,  bloques_data.get(k_p.get(0)).get(0) -3);
    break;
    case 's':
          bloques_data.get(k_p.get(0)).set(1,  bloques_data.get(k_p.get(0)).get(1) +3);
    break;
    case 'd':
          bloques_data.get(k_p.get(0)).set(0,  bloques_data.get(k_p.get(0)).get(0) +3);
    break;
  }
  
}
